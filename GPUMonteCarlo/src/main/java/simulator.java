/**
 * Created by Triangle on 12/12/14.
 */
import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;

public class simulator {
    public StockPath sp;
    public PayOut po;
    public StatsCollector sc;
    public double tolerance = 0.01;
    public double probablity = 0.96;
    public double y;
    public double OptionPrice = 0;
    public int days;

    public simulator(StockPath sp, PayOut po, StatsCollector sc,int days){

        this.sp=sp;
        this.po=po;
        this.sc=sc;
        this.days = days;

        NormalDistribution nd = new NormalDistribution();
        y = nd.inverseCumulativeProbability(probablity);



    }

    public double OptionPriceCalculator(){

        double criteria=1000;

        int N =0;
        double mean =0;

        while(criteria > tolerance || N <5000){

            sc.add(po.getPayout(sp));
            double std =Math.sqrt(sc.getVar());
            mean = sc.getMean();
            N++;
            criteria = std*y/Math.sqrt(N);

        }

        double rate = sp.rate();
        OptionPrice = mean*(Math.exp(-1*days*rate));
        return OptionPrice;

    }





}
