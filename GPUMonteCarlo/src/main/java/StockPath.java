/**
 * Created by Triangle on 12/12/14.
 */
import java.util.*;
import org.joda.time.DateTime;

//The interface for creating StockPath.
//The returned list should be ordered by date
public interface StockPath {
    public List<Pair<DateTime,Double>> getPrices();
    public Pair<DateTime,Double> getLast();
    public double getAvg();
    public double rate();

}
