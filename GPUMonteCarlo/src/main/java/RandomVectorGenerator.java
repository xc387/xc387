/**
 * Created by Triangle on 12/12/14.
 */
//The interface for Random vector generator
public interface RandomVectorGenerator{
    public double[] getVector();
}