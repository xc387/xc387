/**
 * Created by Triangle on 12/12/14.
 */
public class AsianCallOptionPayout implements PayOut{
    public double K;
    public AsianCallOptionPayout(double K){
        this.K = K;
    }

    @Override
    public double getPayout(StockPath path){
        return Math.max(0, path.getAvg() - K);
    }

}
