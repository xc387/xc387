/**
 * Created by Triangle on 12/12/14.
 */
//The interface for calculating the payout function
public interface PayOut{
    public double getPayout(StockPath path);
}