/**
 * Created by Triangle on 12/12/14.
 */
import org.joda.time.DateTime;
//test simulator which integrates every part of the monte carlo project
public class MainTest {

    public static void main(String[] args){

        int N = 10000;
        int batch = 1024*1024;

        GPUGaussianRandomVectorGenerator gpug = new GPUGaussianRandomVectorGenerator(N, batch);
        AntiTheticRandomVectorGenerator atr = new AntiTheticRandomVectorGenerator(gpug);

        DateTime current = new DateTime();
        DateTime endDate = current.plusDays(252);

        //input daily interest rate, sigma, current price, and strike price into gbm stock path generator
        GBMStockPath gsp = new GBMStockPath(0.0001,252,0.01,152.35,current,endDate,atr);


        EuropeanCallOptionPayout ecp = new EuropeanCallOptionPayout(165);

        //test of European call option payout of one path
        System.out.println(ecp.getPayout(gsp));
        System.out.println();

        AsianCallOptionPayout acp = new AsianCallOptionPayout(165);

        //test of Asian call option payout of one path
        System.out.println(acp.getPayout(gsp));
        System.out.println();

        StatsCollector st = new StatsCollector();

        //test of simulator
        st.reset();
        simulator s1 = new simulator(gsp,ecp,st,252);
        System.out.println("price of European Call Option is :" + s1.OptionPriceCalculator());

        st.reset();
        simulator s2 = new simulator(gsp,acp,st,252);
        System.out.println("price of Asian Call Option is :" +s2.OptionPriceCalculator());

    }
}