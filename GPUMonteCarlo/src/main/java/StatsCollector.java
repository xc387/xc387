/**
 * Created by Triangle on 12/12/14.
 */
public class StatsCollector {

    public int N;
    public double mean;
    public double sum;
    public double std;


    public StatsCollector(){
        N=0;
        mean=0;
        sum=0;
        std=0;

    }

    public void add(double payout){
        N++;
        mean = ((double)(N-1)/(double)N)*mean + 1/(double)N*payout;
        sum = (sum*((double)(N-1)) + payout*payout)/(double)N;
    }
    public void reset(){
        this.N =0;
        this.mean = 0.0;
        this.sum = 0.0;
        this.std=0.0;
    }

    public boolean isFinished(double tolerance, double y){
        if (y*Math.sqrt(this.getVar()/N)<tolerance)
            return true;
        else
            return false;


    }
    public int getCounter(){
        return N;
    }

    public double getMean(){
        return mean;
    }
    public double getVar(){
        return sum - mean*mean;
    }
}

