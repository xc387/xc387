/**
 * Created by Triangle on 12/12/14.
 */
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;


public class GBMStockPath implements StockPath{

    private double rate;
    private double sigma;
    private double S0;
    private int N;
    private DateTime startDate;
    private DateTime endDate;
    private RandomVectorGenerator rvg;

    public GBMStockPath(double rate, int N,
                        double sigma, double S0,
                        DateTime startDate, DateTime endDate,
                        RandomVectorGenerator rvg){
        this.startDate = startDate;
        this.endDate = endDate;
        this.rate = rate;
        this.S0 = S0;
        this.sigma = sigma;
        this.N = N;
        this.rvg = rvg;
    }

    @Override
    public List<Pair<DateTime, Double>> getPrices() {
        double[] n = rvg.getVector();
        DateTime current = new DateTime(startDate.getMillis());

        long delta = (endDate.getMillis() - startDate.getMillis())/N;
        //System.out.println(delta);
        List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
        path.add(new Pair<DateTime, Double>(current, S0));
        double price = S0;
        for ( int i=1; i < N; i++){
            current = current.plusMillis((int) delta);
            price = price*Math.exp((rate-sigma*sigma/2)+sigma * n[i-1]);
            path.add(new Pair<DateTime,Double>(current,price));
            //System.out.println(current+" , "+price);
        }
        return path;
    }

    @Override
    public Pair<DateTime,Double> getLast(){
        List<Pair<DateTime, Double>> path = getPrices();
        return path.get(N-1);
    }

    @Override
    public double getAvg(){
        List<Pair<DateTime, Double>> path = getPrices();
        double total = 0;
        for(int i = 0; i < path.size(); i++){
            total += path.get(i).getValue();
        }
        return total/path.size();

    }
    public double rate(){
        return rate;
    }



}

