/**
 * Created by Triangle on 12/12/14.
 */
public class EuropeanCallOptionPayout implements PayOut{
    public double K;
    public EuropeanCallOptionPayout(double K){
        this.K = K;
    }

    @Override
    public double getPayout(StockPath path){

        return Math.max(0, path.getLast().getValue() - K);
    }

}
