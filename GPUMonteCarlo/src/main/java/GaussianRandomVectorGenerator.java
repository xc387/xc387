/**
 * Created by Triangle on 12/12/14.
 */
import java.util.*;

public class GaussianRandomVectorGenerator implements RandomVectorGenerator {
    public double[] grv;
    public int length;
    public Random r = new Random();

    public GaussianRandomVectorGenerator(int n){
        length =n;
        grv = new double[length];

    }

    @Override
    public double[] getVector(){
        for(int i = 0; i<length; i++){
            grv[i] = r.nextGaussian();
        }
        return grv;
    }

}
